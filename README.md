# Ciborium - Lomiri Device Manager

Ciborium is Lomiri's manager for external storage devices.

This component uses udisks2 to search of mountable media and notifies the
user using notify-osd.

Ciborium is implemented in Go and falls into a background services and a
UI component that can be accessed via Lomiri's launcher.

# License

Main application part of Ciborium: See COPYING file.

QML support for the Go language: See qml.v1/LICENSE.

Gettext support for the Go language: See gettext/LICENSE.


## i18n: Translating Lomiri Ciborium into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/ciborium

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.
